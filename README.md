# **_ TRIVIAL FRIKI DE CINE _**

Partiendo de la base que tengo en programación 💻 ( **Html, CSS, JavaScript** ) he creado un juego de Trivial de películas de cine, consiste en poder marcar la opción correcta entre 4 respuestas. Al final de la partida optendras un resultado con el numero de preguntas acertado.
El trabajo de creación de la base de datos de películas lleva a sus espaldas muchas horas de investigación, de visionado de películas , de pensar y de crear.

---

✅ Para ello he creado un `index.html` lo más correcto posible, con una estructura sencilla. He creado algunas etiquetas `<meta/>` que considero que tienen relativa importancia ( _author, description, keywords ..._ ).

✅ El siguiente paso fue ponerse con el archivo `.js` que será el encargado de realizar todo lo necesario para lo que queremos realizar en nuestro juego .

```
let questionH1 = document.getElementById('question');
let answersUL = document.getElementById('answers');
let buttonReset = document.getElementById('reset');
let questionNumber = document.getElementById('question-number');

```

✅ Y para finalizar, he realizado el diseño, gracias al documento `style.css` le doy una imagen visual a la creación, tratando de proporcionarle un aspecto interesante, que no dañe demasiado la vista 👀 .

![Pequeña Imagen de creación](https://i0.wp.com/imgs.hipertextual.com/wp-content/uploads/2016/12/michael-jackson.gif?fit=420%2C315&quality=50&strip=all&ssl=1)

---

> " A veces los viernes Amelie va al cine. Me gusta mirar hacia atrás en la oscuridad y ver la cara de los espectadores. También me gusta descubrir los detalles que nadie más ve. En cambio odio las viejas películas cuando el que conduce nunca mira a la carretera. " — Amelie Poulain

---

### **Autor ✒️ :**

Todo el trabajo ha sido creado por :

- <span style="color:green">Marcos</span> : [LinkedIn](https://es.linkedin.com/in/marcos-v%C3%A1zquez-gonz%C3%A1lez-44379562?trk=people-guest_people_search-card)
  ➡️ Creador de la estructura base en `html`, `javascript` y de diseñar la parte visual de `CSS`.

---


## Funcionalidades :

> - 100% Responsive. </br>
> - Contador de al final de preguntas acertadas. </br>
> - Botón de reseteo. </br>
> - Botón de aleatorio. </br>

---

## Web de la Aplicación ⚙ :

- [Link](https://trivial-cine-dia29.vercel.app/)

---
